# quit without confirmation
define hook-quit
    set confirm off 
end

set pagination off
file app.elf
#file build/dev14-f401.elf
#target remote localhost:3333
target extended-remote localhost:3333

# 2023-09-11 added (7f2)
monitor reset halt 

load
#b main
#b TIM4_IRQHandler
#b app.c:126
#monitor reset init # 2023-09-11 removed
echo RUNNING...\n
c
